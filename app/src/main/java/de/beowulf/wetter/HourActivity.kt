package de.beowulf.wetter

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.databinding.WeatherForecastBinding
import org.json.JSONObject
import java.sql.Time
import java.text.SimpleDateFormat
import java.util.*

class HourActivity : AppCompatActivity() {

    private lateinit var binding: WeatherForecastBinding

    private val hourTime = arrayOfNulls<String>(48)
    private val statusHour = arrayOfNulls<Int>(48)
    private val statusHourText = arrayOfNulls<String>(48)
    private val temp = arrayOfNulls<String>(48)
    private val wind = arrayOfNulls<String>(48)
    private val pressure = arrayOfNulls<String>(48)
    private val humidity = arrayOfNulls<String>(48)
    private val cloudiness = arrayOfNulls<String>(48)
    private val visibility = arrayOfNulls<String>(48)
    private val rainSnow = arrayOfNulls<String>(48)
    private val rainSnowType = arrayOfNulls<String>(48)
    private var x1: Float = 0F
    private var x2: Float = 0F
    private var y1: Float = 0F
    private var y2: Float = 0F
    private val gf = GlobalFunctions()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = WeatherForecastBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        val jsonObj = gf.result()

        binding.ListView.onItemClickListener =
            AdapterView.OnItemClickListener { _, view, _, _ ->
                val moreInfo = view.findViewById<LinearLayout>(R.id.hourMoreInfo)
                if (moreInfo.visibility == View.GONE) {
                    moreInfo.visibility = View.VISIBLE
                } else {
                    moreInfo.visibility = View.GONE
                }
            }

        binding.ListView.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, touchEvent: MotionEvent): Boolean {
                when (touchEvent.action) {
                    MotionEvent.ACTION_DOWN -> {
                        x1 = touchEvent.x
                        y1 = touchEvent.y
                    }
                    MotionEvent.ACTION_UP -> {
                        x2 = touchEvent.x
                        y2 = touchEvent.y
                        if ((x1 - x2) < -150F && y1 - y2 < 100F && y1 - y2 > -100F) {
                            run {
                                val intent = Intent(this@HourActivity, MainActivity::class.java)
                                intent.putExtra("reload", false)
                                startActivity(intent)
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
                                finish()
                            }
                        }
                        if ((x1 - x2) > 150F && y1 - y2 < 100F && y1 - y2 > -100F) {
                            run {
                                val intent = Intent(this@HourActivity, TempGraph::class.java)
                                intent.putExtra("type", "hourly")
                                startActivity(intent)
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                                finish()
                            }
                        }
                        v.performClick()
                    }
                }
                return false
            }
        })

        for (i: Int in 0..47) {
            val hourly: JSONObject = jsonObj.getJSONArray("hourly").getJSONObject(i)
            val hourlyWeather: JSONObject = hourly.getJSONArray("weather").getJSONObject(0)

            val icon: Int = gf.icon(hourlyWeather.getString("icon"))

            /* Populating extracted data into our views */
            hourTime[i] = SimpleDateFormat(getString(R.string.daytime), Locale.ROOT).format(
                Time(
                    hourly.getLong("dt") * 1000
                )
            )
            var tempCut: String = hourly.getString("temp")
            tempCut = tempCut.split(".")[0]
            val windDouble: Double = hourly.getDouble("wind_speed")
            val windDegree: Int = hourly.getInt("wind_deg")
            statusHour[i] = icon
            statusHourText[i] = hourlyWeather.getString("description")
            temp[i] = tempCut + gf.unitTemp()
            wind[i] = gf.changeDecimalSeparator(windDouble) + gf.unitSpeed() + " (${gf.degToCompass(windDegree)})"
            pressure[i] = hourly.getString("pressure") + "hPa"
            humidity[i] = hourly.getString("humidity") + "%"
            cloudiness[i] = hourly.getString("clouds") + "%"
            val visibilityDouble: Double = hourly.getDouble("visibility") / 1000
            visibility[i] = gf.changeDecimalSeparator(visibilityDouble) + "km"
            val precipitation: Double = hourly.getDouble("pop") * 100
            when {
                hourly.has("rain") -> {
                    val rainDouble: Double = hourly.getJSONObject("rain").getDouble("1h")
                    rainSnow[i] = gf.changeDecimalSeparator(rainDouble)
                    rainSnowType[i] = "rain"
                }
                hourly.has("snow") -> {
                    val snowDouble: Double = hourly.getJSONObject("snow").getDouble("1h")
                    rainSnow[i] = gf.changeDecimalSeparator(snowDouble)
                    rainSnowType[i] = "snow"
                }
                else -> {
                    rainSnow[i] = "0"
                    rainSnowType[i] = "rain"
                }
            }
            rainSnow[i] = rainSnow[i] + "mm (${precipitation.toString().split(".")[0]}%)"
        }

        val myListAdapter = MyListHourAdapter(
            this,
            hourTime,
            statusHour,
            statusHourText,
            temp,
            wind,
            pressure,
            humidity,
            cloudiness,
            visibility,
            rainSnow,
            rainSnowType
        )
        binding.ListView.adapter = myListAdapter

    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("reload", false)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
        finish()
    }
}

package de.beowulf.wetter

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.databinding.WeatherForecastBinding
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class DayActivity : AppCompatActivity() {

    private lateinit var binding: WeatherForecastBinding

    private val dayDate = arrayOfNulls<String>(8)
    private val statusDay = arrayOfNulls<Int>(8)
    private val statusDayText = arrayOfNulls<String>(8)
    private val minTemp = arrayOfNulls<String>(8)
    private val maxTemp = arrayOfNulls<String>(8)
    private val sunrise = arrayOfNulls<String>(8)
    private val sunset = arrayOfNulls<String>(8)
    private val wind = arrayOfNulls<String>(8)
    private val pressure = arrayOfNulls<String>(8)
    private val humidity = arrayOfNulls<String>(8)
    private val rainSnow = arrayOfNulls<String>(8)
    private val rainSnowType = arrayOfNulls<String>(8)
    private var x1: Float = 0F
    private var x2: Float = 0F
    private var y1: Float = 0F
    private var y2: Float = 0F
    private val gf = GlobalFunctions()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = WeatherForecastBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        val jsonObj = gf.result()

        binding.ListView.onItemClickListener =
            AdapterView.OnItemClickListener { _, view, _, _ ->
                val moreInfo = view.findViewById<LinearLayout>(R.id.dayMoreInfo)
                if (moreInfo.visibility == View.GONE) {
                    moreInfo.visibility = View.VISIBLE
                } else {
                    moreInfo.visibility = View.GONE
                }
            }

        binding.ListView.setOnTouchListener(
        object : View.OnTouchListener {
            override fun onTouch(v: View, touchEvent: MotionEvent): Boolean {
                when (touchEvent.action) {
                    MotionEvent.ACTION_DOWN -> {
                        x1 = touchEvent.x
                        y1 = touchEvent.y
                    }
                    MotionEvent.ACTION_UP -> {
                        x2 = touchEvent.x
                        y2 = touchEvent.y
                        if ((x1 - x2) < -150F && y1 - y2 < 100F && y1 - y2 > -100F) {
                            run {
                                val intent = Intent(this@DayActivity, TempGraph::class.java)
                                intent.putExtra("type", "daily")
                                startActivity(intent)
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
                                finish()
                            }
                        }
                        if ((x1 - x2) > 150F && y1 - y2 < 100F && y1 - y2 > -100F) {
                            run {
                                val intent = Intent(this@DayActivity, MainActivity::class.java)
                                intent.putExtra("reload", false)
                                startActivity(intent)
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                                finish()
                            }
                        }
                        v.performClick()
                    }
                }
                return false
            }
        })

        for (i: Int in 0..7) {
            val daily: JSONObject = jsonObj.getJSONArray("daily").getJSONObject(i)
            val dailyTemp: JSONObject = daily.getJSONObject("temp")
            val dailyWeather: JSONObject = daily.getJSONArray("weather").getJSONObject(0)

            /* Populating extracted data into our views */
            dayDate[i] = SimpleDateFormat(getString(R.string.daydate), Locale.ROOT).format(
                Date(
                    daily.getLong("dt") * 1000
                )
            )
            if (i == 1) {
                val date: String =
                    SimpleDateFormat(
                        getString(R.string.daydate),
                        Locale.ROOT
                    ).format(Date())
                if (dayDate[0] == date) {
                    dayDate[0] = getString(R.string.today)
                    dayDate[1] = getString(R.string.tomorrow)
                }
            }
            var minTempCut: String = dailyTemp.getString("min")
            minTempCut = minTempCut.split(".")[0]
            var maxTempCut: String = dailyTemp.getString("max")
            maxTempCut = maxTempCut.split(".")[0]
            val windDegree: Int = daily.getInt("wind_deg")
            val windDouble: Double = daily.getDouble("wind_speed")
            val precipitation: Double = daily.getDouble("pop") * 100
            statusDay[i] = gf.icon(dailyWeather.getString("icon"))
            statusDayText[i] = dailyWeather.getString("description")
            minTemp[i] = minTempCut + gf.unitTemp()
            maxTemp[i] = maxTempCut + gf.unitTemp()
            sunrise[i] = SimpleDateFormat(getString(R.string.time), Locale.ROOT).format(
                Date(
                    daily.getLong("sunrise") * 1000
                )
            )
            sunset[i] = SimpleDateFormat(getString(R.string.time), Locale.ROOT).format(
                Date(
                    daily.getLong("sunset") * 1000
                )
            )
            wind[i] = gf.changeDecimalSeparator(windDouble) + gf.unitSpeed() + " (${gf.degToCompass(windDegree)})"
            pressure[i] = daily.getString("pressure") + "hPa"
            humidity[i] = daily.getString("humidity") + "%"
            when {
                daily.has("rain") -> {
                    val rainDouble: Double = daily.getDouble("rain")
                    rainSnow[i] = gf.changeDecimalSeparator(rainDouble)
                    rainSnowType[i] = "rain"
                }
                daily.has("snow") -> {
                    val snowDouble: Double = daily.getDouble("snow")
                    rainSnow[i] = gf.changeDecimalSeparator(snowDouble)
                    rainSnowType[i] = "snow"
                }
                else -> {
                    rainSnow[i] = "0"
                    rainSnowType[i] = "rain"
                }
            }
            rainSnow[i] = rainSnow[i] + "mm (${precipitation.toString().split(".")[0]}%)"
        }

        val myListAdapter = MyListDayCitiesAdapter(
            this,
            dayDate,
            statusDay,
            statusDayText,
            minTemp,
            maxTemp,
            sunrise,
            sunset,
            wind,
            pressure,
            humidity,
            rainSnow,
            rainSnowType
        )
        binding.ListView.adapter = myListAdapter

    }

    override fun onBackPressed() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("reload", false)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        finish()
    }
}

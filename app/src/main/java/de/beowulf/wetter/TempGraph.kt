package de.beowulf.wetter

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import de.beowulf.wetter.databinding.ActivityTempGraphBinding
import org.json.JSONObject

class TempGraph : AppCompatActivity() {

    private lateinit var binding: ActivityTempGraphBinding

    private var type: String = "hourly"
    private val temp = arrayOfNulls<Int>(8)
    private val precipitation = arrayOfNulls<Int>(8)
    private var x1: Float = 0F
    private var x2: Float = 0F
    private var y1: Float = 0F
    private var y2: Float = 0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTempGraphBinding.inflate(layoutInflater)
        setContentView(binding.root)

        type = intent.getStringExtra("type").toString()

        binding.forecastType.text = if (type == "daily") {
            getString(R.string.daily)
        } else {
            getString(R.string.hourly)
        }

        val graphView = findViewById<TempGraphAdapter>(R.id.graph_view)
        graphView.setData(temp())
        val graphView2 = findViewById<TempGraphAdapter>(R.id.graph_view2)
        graphView2.setData(precipitation())
    }

    private fun temp(): List<DataPoint> {
        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)

        val result: String = settings.getString("result", "").toString()

        val jsonObj = JSONObject(result)

        if (type == "daily") {
            for (i: Int in 0..7) {
                temp[i] = (jsonObj.getJSONArray(type).getJSONObject(i).getJSONObject("temp").getInt("max") + jsonObj.getJSONArray(type).getJSONObject(i).getJSONObject("temp").getInt("min")) / 2
            }
        } else {
            for (i: Int in 0..7) {
                temp[i] = jsonObj.getJSONArray(type).getJSONObject(i).getInt("temp")
            }
        }
        return (0..7).map {
            DataPoint(it, temp[it]!!)
        }
    }

    private fun precipitation(): List<DataPoint> {
        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)

        val result: String = settings.getString("result", "").toString()

        val jsonObj = JSONObject(result)

        for (i: Int in 0..7) {
            precipitation[i] = (jsonObj.getJSONArray(type).getJSONObject(i).getDouble("pop") * 100).toInt()
        }
        return (0..7).map {
            DataPoint(it, precipitation[it]!!)
        }
    }

    override fun onTouchEvent(touchEvent: MotionEvent): Boolean {
        when (touchEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                x1 = touchEvent.x
                y1 = touchEvent.y
            }
            MotionEvent.ACTION_UP -> {
                x2 = touchEvent.x
                y2 = touchEvent.y
                if ((x1 - x2) < -150F && y1-y2 < 100F && y1-y2 > -100F) {
                    run {
                        if (type == "daily") {
                            val intent = Intent(this, TempGraph::class.java)
                            intent.putExtra("type", "hourly")
                            startActivity(intent)
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
                            finish()
                        } else {
                            val intent = Intent(this, HourActivity::class.java)
                            startActivity(intent)
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
                            finish()
                        }
                    }
                }
                if ((x1 - x2) > 150F && y1-y2 < 100F && y1-y2 > -100F) {
                    run {
                        if (type == "hourly") {
                            val intent = Intent(this, TempGraph::class.java)
                            intent.putExtra("type", "daily")
                            startActivity(intent)
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                            finish()
                        } else {
                            val intent = Intent(this, DayActivity::class.java)
                            startActivity(intent)
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                            finish()
                        }
                    }
                }
            }
        }
        return false
    }

    override fun onBackPressed() {
        if (type == "hourly") {
            val intent = Intent(this, HourActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            finish()
        } else {
            val intent = Intent(this, DayActivity::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            finish()
        }
    }
}
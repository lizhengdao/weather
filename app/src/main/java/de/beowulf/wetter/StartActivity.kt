package de.beowulf.wetter

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.databinding.ActivityStartBinding
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.URL
import java.util.*
import java.util.concurrent.Executors

class StartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStartBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)

        binding.CreatedBy.movementMethod = LinkMovementMethod.getInstance()
        binding.ReportError.movementMethod = LinkMovementMethod.getInstance()

        var lat: String = settings.getString("lat", "")!!
        var city: String = settings.getString("city", "")!!
        var unit: Int = settings.getInt("unit", 0)
        var api: String = settings.getString("api", "")!!
        val change: Boolean = intent.getBooleanExtra("change", false)

        val units = resources.getStringArray(R.array.units)
        binding.Unit.adapter = ArrayAdapter(this, R.layout.spinner, units)

        if (change) {
            binding.City.setText(city)
            binding.Unit.setSelection(unit)
            if (api != "fb792651e5a6d41199171c6c41eca22b")
                binding.Api.setText(api)
        }

        binding.ApiText.setOnClickListener {
            val uriUrl: Uri = Uri.parse("https://home.openweathermap.org/api_keys")
            val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
            startActivity(launchBrowser)
        }

        if (lat != "" && !change) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else binding.Submit.setOnClickListener {
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE
            val executor = Executors.newScheduledThreadPool(5)

            doAsync(executorService = executor) {
                val result: String? = try {
                    city = binding.City.text.toString()
                    api = binding.Api.text.toString()
                    if (api == "")
                        api = "fb792651e5a6d41199171c6c41eca22b"
                    URL("https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$api").readText(Charsets.UTF_8)
                } catch (e: Exception) {
                    null
                }
                uiThread {
                    if (result != null) {
                        val jsonObj = JSONObject(result)
                        val coord: JSONObject = jsonObj.getJSONObject("coord")
                        val sys: JSONObject = jsonObj.getJSONObject("sys")

                        val lon: String = coord.getString("lon")
                        lat = coord.getString("lat")
                        city = jsonObj.getString("name") + ", " + sys.getString("country")
                        unit = binding.Unit.selectedItemPosition
                        api = binding.Api.text.toString()
                        if (api == "")
                            api = "fb792651e5a6d41199171c6c41eca22b"
                        success(lon, lat, city, unit, api)
                    } else {
                        Toast.makeText(this@StartActivity, R.string.error_occurred_start, Toast.LENGTH_LONG).show()
                    }
                    binding.loader.visibility = View.GONE
                    binding.mainContainer.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun success(lon: String, lat: String, city: String, unit: Int, api: String) {
        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putString("lon", lon)
        editor.putString("lat", lat)
        editor.putString("city", city)
        editor.putInt("unit", unit)
        editor.putString("api", api)
        editor.apply()
        if (api == "fb792651e5a6d41199171c6c41eca22b") {
            Toast.makeText(this, getString(R.string.ErrorStandardAPI), Toast.LENGTH_LONG).show()
        }
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
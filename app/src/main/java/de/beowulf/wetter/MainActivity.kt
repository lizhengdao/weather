package de.beowulf.wetter

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.R.*
import de.beowulf.wetter.databinding.ActivityMainBinding
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var reload: Boolean = true
    private var city: String? = ""
    private var x1: Float = 0F
    private var x2: Float = 0F
    private var y1: Float = 0F
    private var y2: Float = 0F
    private val gf = GlobalFunctions()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)
        reload = intent.getBooleanExtra("reload", true)

        city = settings.getString("city", "")
        val api = settings.getString("api", "")
        if (api == "fb792651e5a6d41199171c6c41eca22b") {
            Toast.makeText(this, getString(string.ErrorStandardAPI), Toast.LENGTH_LONG).show()
        }

        binding.SunriseText.isSelected = true
        binding.SunsetText.isSelected = true
        binding.HumidityText.isSelected = true

        if (reload) {
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE
            val executor = Executors.newScheduledThreadPool(5)

            doAsync(executorService = executor) {
                val result: String? = try {
                    URL(gf.url("normal", "")).readText(Charsets.UTF_8)
                } catch (e: Exception) {
                    null
                }
                uiThread {
                    if (result != null) {
                        gf.setResult(result)
                    } else {
                        Toast.makeText(this@MainActivity, string.error_occurred, Toast.LENGTH_SHORT).show()
                    }
                    loadData()
                }
            }
        } else {
            loadData()
        }

        binding.Address.setOnClickListener {
            val intent = Intent(this, StartActivity::class.java)
            intent.putExtra("change", true)
            startActivity(intent)
            finish()
        }
    }

    private fun loadData() {
        val jsonObj = gf.result()
        val current: JSONObject = jsonObj.getJSONObject("current")
        val currentWeather: JSONObject =
            current.getJSONArray("weather").getJSONObject(0)
        val daily: JSONObject =
            jsonObj.getJSONArray("daily").getJSONObject(0).getJSONObject("temp")

        val updatedAt: Long = current.getLong("dt")
        val updatedAtText: String =
            getString(string.updated_at) + " " + SimpleDateFormat(
                getString(string.date),
                Locale.ROOT
            ).format(Date(updatedAt * 1000))
        val tempDouble: Double = current.getDouble("temp")
        val temp: String = gf.changeDecimalSeparator(tempDouble) + gf.unitTemp()
        val tempMinCut: String = "Min Temp: " + daily.getString("min").split(".")[0] + gf.unitTemp()
        val tempMaxCut: String = "Max Temp: " + daily.getString("max").split(".")[0] + gf.unitTemp()
        val pressure: String = current.getString("pressure") + "hPa"
        val humidity: String = current.getString("humidity") + "%"
        val rainSnowPop: Double = jsonObj.getJSONArray("hourly").getJSONObject(0).getDouble("pop") * 100
        when {
            current.has("rain") -> {
                val rainDouble: Double = current.getJSONObject("rain").getDouble("1h")
                val rain = "${gf.changeDecimalSeparator(rainDouble)}mm"
                binding.Rain.text = rain
                val rainText: String = getString(string.rain) + " (${rainSnowPop.toString().split(".")[0]}%)"
                binding.RainText.text = rainText
                binding.RainLL.visibility = View.VISIBLE
                binding.SnowLL.visibility = View.GONE
            }
            current.has("snow") -> {
                val snowDouble: Double = current.getJSONObject("snow").getDouble("1h")
                val snow = "${gf.changeDecimalSeparator(snowDouble)}mm"
                binding.Snow.text = snow
                val snowText = getString(string.snow) + " (${rainSnowPop.toString().split(".")[0]}%)"
                binding.SnowText.text = snowText
                binding.SnowLL.visibility = View.VISIBLE
                binding.RainLL.visibility = View.GONE
            }
            else -> {
                val rain = "0mm"
                binding.Rain.text = rain
                val rainText = getString(string.rain) + " (${rainSnowPop.toString().split(".")[0]}%)"
                binding.RainText.text = rainText
                binding.RainLL.visibility = View.VISIBLE
                binding.SnowLL.visibility = View.GONE
            }
        }

        val sunrise: Long = current.getLong("sunrise")
        val sunset: Long = current.getLong("sunset")
        val windSpeedDouble: Double = current.getDouble("wind_speed")
        val windSpeed = gf.changeDecimalSeparator(windSpeedDouble) + gf.unitSpeed()
        val windDegree: Int = current.getInt("wind_deg")
        val weatherDescription: String = currentWeather.getString("description")
        val icon: Int = gf.icon(currentWeather.getString("icon"))

        /* Populating extracted data into our views */
        binding.Address.text = city
        binding.UpdatedAt.text = updatedAtText
        binding.ImgStatus.setImageResource(icon)
        binding.Status.text = weatherDescription.capitalize(Locale.ROOT)
        binding.Temp.text = temp
        binding.TempMin.text = tempMinCut
        binding.TempMax.text = tempMaxCut
        binding.Sunrise.text = SimpleDateFormat(getString(string.time), Locale.ROOT).format(
            Date(
                sunrise * 1000
            )
        )
        binding.Sunset.text = SimpleDateFormat(getString(string.time), Locale.ROOT).format(
            Date(
                sunset * 1000
            )
        )
        val windText = getString(string.wind) + " (${gf.degToCompass(windDegree)})"
        binding.WindText.text = windText
        binding.Wind.text = windSpeed
        binding.Pressure.text = pressure
        binding.Humidity.text = humidity
        binding.loader.visibility = View.GONE
        binding.mainContainer.visibility = View.VISIBLE
    }

    override fun onTouchEvent(touchEvent: MotionEvent): Boolean {
        when (touchEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                x1 = touchEvent.x
                y1 = touchEvent.y
            }
            MotionEvent.ACTION_UP -> {
                x2 = touchEvent.x
                y2 = touchEvent.y
                if ((x1 - x2) < -150F && (y1-y2) < 100F && (y1-y2) > -100F) {
                    run {
                        val intent = Intent(this, DayActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(anim.slide_in_left, anim.slide_out_right)
                        finish()
                    }
                }
                if ((x1 - x2) > 150F && (y1-y2) < 100F && (y1-y2) > -100F) {
                    run {
                        val intent = Intent(this, HourActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(anim.slide_in_right, anim.slide_out_left)
                        finish()
                    }
                }
                if ((y1-y2) > 100F && (x1 - x2) < 150F && (x1 - x2) > -150F) {
                    val intent = Intent(this, MultiCityActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(anim.slide_in_down, anim.slide_out_up)
                    finish()
                }
            }
        }
        return false
    }
}
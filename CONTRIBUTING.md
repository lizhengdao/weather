# Contributing

## Reporting issues

If you find an issue in the client, you can use our [Issue
Tracker](https://gitlab.com/BeowuIf/wetter/-/issues). Make sure that it
hasn't yet been reported by searching first.

Remember to include the following information:

* Android version
* Device model
* App version
* Steps to reproduce the issue

## Translating

The strings are translated directly in Android Studio.
